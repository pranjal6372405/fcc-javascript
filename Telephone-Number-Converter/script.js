document.getElementById('check-btn').addEventListener('click', checkPhoneNumber);
document.getElementById('clear-btn').addEventListener('click', clearResults);

function checkPhoneNumber() {
  const userInput = document.getElementById('user-input').value;
  const resultsDiv = document.getElementById('results-div');

  if (userInput === '') {
    alert('Please provide a phone number');
    return;
  }

  const phoneNumberRegex = /^(1\s?)?(\(\d{3}\)|\d{3})([\s\-]?)\d{3}([\s\-]?)\d{4}$/;
  if (phoneNumberRegex.test(userInput)) {
    resultsDiv.textContent = `Valid US number: ${userInput}`;
  } else {
    resultsDiv.textContent = `Invalid US number: ${userInput}`;
  }
}

function clearResults() {
  document.getElementById('results-div').textContent = '';
}