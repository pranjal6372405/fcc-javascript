/* file: script.js */
const checkButton = document.getElementById('check-btn');
const result = document.getElementById('result');
const textInput = document.getElementById('text-input');

function palindrome(str) {
  // Convert str to lowercase and remove non-alphanumeric characters
  const alphanumericOnly = str.toLowerCase().match(/[a-z0-9]/g).join('');
  const reverseAlpha = alphanumericOnly.split('').reverse().join('');

  // Determine if palindrome
  if (alphanumericOnly === reverseAlpha) {
    result.innerText = `${str} is a palindrome`;
  } else {
    result.innerText = `${str} is not a palindrome`;
  }
}

// Check button for no input
checkButton.addEventListener('click', () => {
  if (textInput.value === "") {
    alert("Please input a value");
  } else {
    palindrome(textInput.value);
  }
});
