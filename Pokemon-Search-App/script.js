const apiUrl = 'https://pokeapi-proxy.freecodecamp.rocks/api/pokemon'

const pokemonImgProxi = 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Pok%C3%A9mon_Trading_Card_Game_logo.svg/1200px-Pok%C3%A9mon_Trading_Card_Game_logo.svg.png'

const colours = {
	normal: '#A8A77A',
	fire: '#EE8130',
	water: '#6390F0',
	electric: '#F7D02C',
	grass: '#7AC74C',
	ice: '#96D9D6',
	fighting: '#C22E28',
	poison: '#A33EA1',
	ground: '#E2BF65',
	flying: '#A98FF3',
	psychic: '#F95587',
	bug: '#A6B91A',
	rock: '#B6A136',
	ghost: '#735797',
	dragon: '#6F35FC',
	dark: '#705746',
	steel: '#B7B7CE',
	fairy: '#D685AD',
};

const backgrNameId = document.getElementsByClassName('pokemonname')

const searchInput = document.getElementById('search-input')

const pokemonName = document.getElementById('pokemon-name')

const pokemonImg = document.getElementById('imgcontainer')

const pokemonId = document.getElementById('pokemon-id')

const pokemonWeight = document.getElementById('weight')

const pokemonHeight = document.getElementById('height')

const pokemonTypes = document.getElementsByClassName('types')

const pokemonHp = document.getElementById('hp')

const pokemonAttack = document.getElementById('attack')

const pokemonDefense = document.getElementById('defense')

const pokemonSpecialAttack = document.getElementById('special-attack')

const pokemonSpecialDefense = document.getElementById('special-defense')

const pokemonSpeed = document.getElementById('speed')

const cardBorder = document.getElementById('card-border')

const searchButton = document.getElementById('search-button')

pokemonImg.innerHTML = `<img id='sprite' src='${pokemonImgProxi}' alt='pokemon-image' class='pokemonimage'/>`

const convertHexToRgb = (hex) => {
	hex = hex.slice(1)
	return [Number.parseInt(hex.slice(0,2), 16), Number.parseInt(hex.slice(2,4), 16), Number.parseInt(hex.slice(4,6), 16)]
}

const contrastColor = (color) => {
	let luminance = Number.parseFloat((0.299*color[0] + 0.587*color[1] + 0.114*color[2])/255)
	return luminance>0.5 ? [0,0,0] : [255,255,255]
}

const lookForPokemon = async (searchStr) => {
  if (searchStr) {
    let searchResult = await getAllPokemons(apiUrl)
    searchResult = searchResult.results.filter(({name, id}) => {return name == searchStr.toLowerCase() || id == searchStr})
  if (searchResult.length == 1) {
    showPokemon(searchResult[0].url.replace(/http/, 'https').slice(0,-1))
    } else if (searchResult.length == 0) {
			alert('Pokémon not found')
		}
  }
}

const getAllPokemons = async (url) => {

  const response = await fetch(url)

  if (!response.ok) {
    throw new Error(`HTTP error! status: ${response.status}`);
  } else {
    return await response.json()
  }
}

const showPokemon = async (url) => {
  const pokemon = await getAllPokemons(url)
	let backColor=convertHexToRgb(colours['electric'])
	let textColor=contrastColor(backColor)
	if (pokemon.types[0].type.name) {
		backColor=convertHexToRgb(colours[pokemon.types[0].type.name])
	}
	let backColorHTML='rgb(' + backColor.join(', ') + ')'
	let textColorHTML='rgb(' + textColor.join(', ') + ')'
	for (let p=0; p<=3; p++) {
		backgrNameId[p].style.backgroundColor = backColorHTML
		backgrNameId[p].style.color = textColorHTML
	}
	cardBorder.style.borderColor = backColorHTML
	pokemonName.innerText = pokemon.name.toUpperCase()
	pokemonId.innerText = pokemon.id
	pokemonWeight.innerText = 'Weight: ' + pokemon.weight
	pokemonHeight.innerText = 'Height: ' + pokemon.height
	pokemonTypes[0].innerHTML = ''
pokemonImg.innerHTML = ''
	for (let typeName of pokemon.types) {
		pokemonTypes[0].innerHTML += '<h4>' + typeName.type.name.toUpperCase() + '</h4>'
	}
	pokemonHp.innerText = pokemon.stats[0].base_stat
	pokemonAttack.innerText = pokemon.stats[1].base_stat
	pokemonDefense.innerText = pokemon.stats[2].base_stat
	pokemonSpecialAttack.innerText = pokemon.stats[3].base_stat
	pokemonSpecialDefense.innerText = pokemon.stats[4].base_stat
	pokemonSpeed.innerText = pokemon.stats[5].base_stat
	pokemonImg.innerHTML = `<img id="sprite" src="${pokemon.sprites.front_default}" alt="pokemon-image" class="pokemonimage" />`
}

searchButton.addEventListener('click', () => {
   lookForPokemon(searchInput.value)
})

searchInput.addEventListener('keypress', (e) => {
	if (e.key === 'Enter') {
		searchButton.click()
	}
})